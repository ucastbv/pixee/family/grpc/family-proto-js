# Tellie Family gRPC JS stubs

This repository provides a npm package that can be included in NodeJS projects.

IMPORTANT: Contents of the src folder are autogenerated by the family-proto CI pipeline.

Every commit in this repository automatically triggers creation of a new npm package on this project's package registry.
