// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var friendship_pb = require('./friendship_pb.js');
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');

function serialize_google_protobuf_Empty(arg) {
  if (!(arg instanceof google_protobuf_empty_pb.Empty)) {
    throw new Error('Expected argument of type google.protobuf.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_google_protobuf_Empty(buffer_arg) {
  return google_protobuf_empty_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_FriendshipCreatedRequest(arg) {
  if (!(arg instanceof friendship_pb.FriendshipCreatedRequest)) {
    throw new Error('Expected argument of type tellie.family.FriendshipCreatedRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_FriendshipCreatedRequest(buffer_arg) {
  return friendship_pb.FriendshipCreatedRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_FriendshipReactivatedRequest(arg) {
  if (!(arg instanceof friendship_pb.FriendshipReactivatedRequest)) {
    throw new Error('Expected argument of type tellie.family.FriendshipReactivatedRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_FriendshipReactivatedRequest(buffer_arg) {
  return friendship_pb.FriendshipReactivatedRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_FriendshipRemovedRequest(arg) {
  if (!(arg instanceof friendship_pb.FriendshipRemovedRequest)) {
    throw new Error('Expected argument of type tellie.family.FriendshipRemovedRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_FriendshipRemovedRequest(buffer_arg) {
  return friendship_pb.FriendshipRemovedRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_FriendshipUpdatedRequest(arg) {
  if (!(arg instanceof friendship_pb.FriendshipUpdatedRequest)) {
    throw new Error('Expected argument of type tellie.family.FriendshipUpdatedRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_FriendshipUpdatedRequest(buffer_arg) {
  return friendship_pb.FriendshipUpdatedRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var FriendshipService = exports.FriendshipService = {
  notifyCreated: {
    path: '/tellie.family.Friendship/NotifyCreated',
    requestStream: false,
    responseStream: false,
    requestType: friendship_pb.FriendshipCreatedRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_FriendshipCreatedRequest,
    requestDeserialize: deserialize_tellie_family_FriendshipCreatedRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
  notifyUpdated: {
    path: '/tellie.family.Friendship/NotifyUpdated',
    requestStream: false,
    responseStream: false,
    requestType: friendship_pb.FriendshipUpdatedRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_FriendshipUpdatedRequest,
    requestDeserialize: deserialize_tellie_family_FriendshipUpdatedRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
  notifyRemoved: {
    path: '/tellie.family.Friendship/NotifyRemoved',
    requestStream: false,
    responseStream: false,
    requestType: friendship_pb.FriendshipRemovedRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_FriendshipRemovedRequest,
    requestDeserialize: deserialize_tellie_family_FriendshipRemovedRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
  notifyReactivated: {
    path: '/tellie.family.Friendship/NotifyReactivated',
    requestStream: false,
    responseStream: false,
    requestType: friendship_pb.FriendshipReactivatedRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_FriendshipReactivatedRequest,
    requestDeserialize: deserialize_tellie_family_FriendshipReactivatedRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
};

exports.FriendshipClient = grpc.makeGenericClientConstructor(FriendshipService);
