// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var conversation_pb = require('./conversation_pb.js');
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');

function serialize_google_protobuf_Empty(arg) {
  if (!(arg instanceof google_protobuf_empty_pb.Empty)) {
    throw new Error('Expected argument of type google.protobuf.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_google_protobuf_Empty(buffer_arg) {
  return google_protobuf_empty_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_MessageSentRequest(arg) {
  if (!(arg instanceof conversation_pb.MessageSentRequest)) {
    throw new Error('Expected argument of type tellie.family.MessageSentRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_MessageSentRequest(buffer_arg) {
  return conversation_pb.MessageSentRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var ConversationService = exports.ConversationService = {
  notifyMessageSent: {
    path: '/tellie.family.Conversation/NotifyMessageSent',
    requestStream: false,
    responseStream: false,
    requestType: conversation_pb.MessageSentRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_MessageSentRequest,
    requestDeserialize: deserialize_tellie_family_MessageSentRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
};

exports.ConversationClient = grpc.makeGenericClientConstructor(ConversationService);
