// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var endUser_pb = require('./endUser_pb.js');
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');

function serialize_google_protobuf_Empty(arg) {
  if (!(arg instanceof google_protobuf_empty_pb.Empty)) {
    throw new Error('Expected argument of type google.protobuf.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_google_protobuf_Empty(buffer_arg) {
  return google_protobuf_empty_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_EndUserDeactivatedRequest(arg) {
  if (!(arg instanceof endUser_pb.EndUserDeactivatedRequest)) {
    throw new Error('Expected argument of type tellie.family.EndUserDeactivatedRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_EndUserDeactivatedRequest(buffer_arg) {
  return endUser_pb.EndUserDeactivatedRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_EndUserReactivatedRequest(arg) {
  if (!(arg instanceof endUser_pb.EndUserReactivatedRequest)) {
    throw new Error('Expected argument of type tellie.family.EndUserReactivatedRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_EndUserReactivatedRequest(buffer_arg) {
  return endUser_pb.EndUserReactivatedRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_SettingsChangedRequest(arg) {
  if (!(arg instanceof endUser_pb.SettingsChangedRequest)) {
    throw new Error('Expected argument of type tellie.family.SettingsChangedRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_SettingsChangedRequest(buffer_arg) {
  return endUser_pb.SettingsChangedRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


var EndUserService = exports.EndUserService = {
  notifyDeactivatedEndUserAndTheirFriends: {
    path: '/tellie.family.EndUser/NotifyDeactivatedEndUserAndTheirFriends',
    requestStream: false,
    responseStream: false,
    requestType: endUser_pb.EndUserDeactivatedRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_EndUserDeactivatedRequest,
    requestDeserialize: deserialize_tellie_family_EndUserDeactivatedRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
  notifyReactivatedEndUserAndTheirFriends: {
    path: '/tellie.family.EndUser/NotifyReactivatedEndUserAndTheirFriends',
    requestStream: false,
    responseStream: false,
    requestType: endUser_pb.EndUserReactivatedRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_EndUserReactivatedRequest,
    requestDeserialize: deserialize_tellie_family_EndUserReactivatedRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
  notifyEndUserOfChangedSettings: {
    path: '/tellie.family.EndUser/NotifyEndUserOfChangedSettings',
    requestStream: false,
    responseStream: false,
    requestType: endUser_pb.SettingsChangedRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_SettingsChangedRequest,
    requestDeserialize: deserialize_tellie_family_SettingsChangedRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
};

exports.EndUserClient = grpc.makeGenericClientConstructor(EndUserService);
