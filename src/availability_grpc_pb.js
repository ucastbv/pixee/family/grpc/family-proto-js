// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var availability_pb = require('./availability_pb.js');
var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js');

function serialize_google_protobuf_Empty(arg) {
  if (!(arg instanceof google_protobuf_empty_pb.Empty)) {
    throw new Error('Expected argument of type google.protobuf.Empty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_google_protobuf_Empty(buffer_arg) {
  return google_protobuf_empty_pb.Empty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_tellie_family_ChangeAvailabilityRequest(arg) {
  if (!(arg instanceof availability_pb.ChangeAvailabilityRequest)) {
    throw new Error('Expected argument of type tellie.family.ChangeAvailabilityRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_tellie_family_ChangeAvailabilityRequest(buffer_arg) {
  return availability_pb.ChangeAvailabilityRequest.deserializeBinary(new Uint8Array(buffer_arg));
}


// The Availability service definitions.
var AvailabilityService = exports.AvailabilityService = {
  notifyUserDoNotDisturbEnabled: {
    path: '/tellie.family.Availability/NotifyUserDoNotDisturbEnabled',
    requestStream: false,
    responseStream: false,
    requestType: availability_pb.ChangeAvailabilityRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_ChangeAvailabilityRequest,
    requestDeserialize: deserialize_tellie_family_ChangeAvailabilityRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
  notifyUserDoNotDisturbDisabled: {
    path: '/tellie.family.Availability/NotifyUserDoNotDisturbDisabled',
    requestStream: false,
    responseStream: false,
    requestType: availability_pb.ChangeAvailabilityRequest,
    responseType: google_protobuf_empty_pb.Empty,
    requestSerialize: serialize_tellie_family_ChangeAvailabilityRequest,
    requestDeserialize: deserialize_tellie_family_ChangeAvailabilityRequest,
    responseSerialize: serialize_google_protobuf_Empty,
    responseDeserialize: deserialize_google_protobuf_Empty,
  },
};

exports.AvailabilityClient = grpc.makeGenericClientConstructor(AvailabilityService);
